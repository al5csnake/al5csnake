#include "snake-game.h"
/** @file */
#include "../engine/display/engi-display.h"
#include "../engine/event/engi-event-queue.h"
#include "../engine/core/engi-core.h"
#include "../engine/game-loop/engi-game-loop.h"
#include "../engine/scenes/scene/engi-scene.h"
#include "../engine/scenes/manager/engi-scenes-manager.h"

#include <malloc.h>

#include "scenes/menu/game-menu-scene.h"

void run_snake_game() {
    //creates display
    engi_display *display = create_engi_display(640, 480);
    //creates event queue
    engi_event_queue *queue = create_engi_event_queue(display);

    //creates scenes_manager
    engi_scenes_manager *scenes_manager = create_engi_scenes_manager();

    //pushes main_menu scene
    push_scene_to_engi_scenes_manager(scenes_manager,
                                      create_game_menu_scene(queue, scenes_manager));
    

    //destroys scenes_manager
    destroy_engi_scenes_manager(scenes_manager);
    //destroy display
    destroy_engi_display(display);
    //destroy event queue
    destroy_engi_event_queue(queue);
}