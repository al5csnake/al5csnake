#pragma once
/** @file */
#include "../../../../containers/lcthw-list.h"

#include <stdbool.h>
#include <allegro5/allegro.h>

#include "apple-component.h"

#include "limiter.h"


#include "snake-data.h"

/// @return spawned snake with given @param segments_count and @param way
snake spawn_snake(unsigned segments_count, way way);

/// @brief frees data associated to given @param snake
void free_snake(snake snake);

/// @brief performs move on given @param snake
void move_snake(snake snake);

/// @brief tries to set given @param snake way to @param way
void set_snake_way(snake *snake, way way);

/// @brief checks if @param snake is in @param limiter_box
bool test_snake_in_box(snake snake, limiter limiter_box);

/// @draws given @param @snake
void draw_snake(snake snake);

enum {
    /// @brief snake is being moved every 5th loop encirclement
    SNAKE_MOVER_TICKS_FOR_EVENT = 10
};

/// @brief snake move timer for moving given @parma snake
void update_snake_mover(snake snake);

/// @brief @param snake eats every apple from @param apples whenever his head touches one of them
void find_and_eat_touched_apple(snake *snake, apples apples);

// @brief checks if @param snake touches tail with his head
bool snake_touches_himself(snake snake);