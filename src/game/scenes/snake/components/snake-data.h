#pragma once
/** @file */

/**
* @brief smartly represented up/down/left/right ways
*/
typedef enum {
    way_vertical = 1,
    way_up = -way_vertical, way_down = way_vertical,

    way_horizontal = 2,
    way_left = -way_horizontal, way_right = way_horizontal
} way;

/**
@brief snake with its body, segment bitmap, way and collected points
*/
typedef struct {
    List *body;
    ALLEGRO_BITMAP *seg_bitmap;
    way way;
    int points;
} snake;

enum {
    /// @brief snake segment size in pixels
    SNAKE_SEGMENT_SIZE = 32
};


/// @brief snake_body_segment represents segment coordinates
typedef struct {
    int x;
    int y;
} snake_body_segment;