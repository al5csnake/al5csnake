#include "snake-component.h"
/** @file */
#include <stdlib.h>


snake spawn_snake(unsigned segments_count, way way) {
    //creates snake
    snake result = {
        //creates body list
        .body = List_create(),
        //sets way
        .way = way,
        //loads segment bitmap
        .seg_bitmap = al_load_bitmap("resources/images/snake_segment.jpg"),
        //initializes points
        .points = 0
    };

    //spawns snake segments
    unsigned i = 1;
    for(i; i <= segments_count; ++i) {
        snake_body_segment *sbs = malloc(sizeof(snake_body_segment));
        sbs->x = 1;
        sbs->y = segments_count-i+1;
        List_push(result.body, sbs);
    }

    //returns snake
    return result;
}

void free_snake(snake snake) {
    //destroys snake body
    List_clear_destroy(snake.body);
    //destroys snake segment bitmap
    al_destroy_bitmap(snake.seg_bitmap);
}

void move_snake(snake snake) {
    snake_body_segment *head = List_first(snake.body);
    //gets last segment
    snake_body_segment *new_head = List_pop(snake.body);
    //sets it to head position
    *new_head = *head;

    //moves head
    if(abs(snake.way) == way_vertical) {
        new_head->y += snake.way / way_vertical;
    }
    else if(abs(snake.way) == way_horizontal) {
        new_head->x += snake.way / way_horizontal;
    }

    //pushes new head to front (tada, the movement is done)
    List_unshift(snake.body, new_head);
}

void set_snake_way(snake *snake, way way) {
    //if snake is not on the opposite of given way
    if(snake->way != way*-1) {
        //sets given way
        snake->way = way;
    }
}

bool test_snake_in_box(snake snake, limiter limiter_box) {
    snake_body_segment *head = List_first(snake.body);

    bool horizontal = head->x >= limiter_box.min_x && head->x < limiter_box.max_x;
    bool vertical = head->y >= limiter_box.min_y && head->y < limiter_box.max_y;

    //returns true if snake is in box
    return horizontal && vertical;
}

void draw_snake(snake snake) {
    //draws every snake segment
    LIST_FOREACH(snake.body, first, next, current) {
        snake_body_segment *seg = current->value;
        al_draw_bitmap(snake.seg_bitmap, seg->x*SNAKE_SEGMENT_SIZE, seg->y*SNAKE_SEGMENT_SIZE, 0);
    }
}

void update_snake_mover(snake snake) {
    static int counter = 0;
    counter = (counter % SNAKE_MOVER_TICKS_FOR_EVENT) + 1;
    if(counter == 1) {
        move_snake(snake);
    }
}

void find_and_eat_touched_apple(snake *snake, apples apples) {
    snake_body_segment *head = List_first(snake->body);
    LIST_FOREACH(apples.list, first, next, cur) {
        apple *appl = cur->value;
        //if position matches
        if(appl->x == head->x && appl->y == head->y) {
            //adds points
            snake->points += appl->points;

            //destroys eaten apple
            free(List_remove(apples.list, cur));

            //adds new segment to snake
            snake_body_segment *last = List_last(snake->body);
            snake_body_segment *added = malloc(sizeof(snake_body_segment));
            *added = *last;
            List_push(snake->body, added);
            //end function
            return;
        }
    }
}

bool snake_touches_himself(snake snake) {
    snake_body_segment *head = List_first(snake.body);

    LIST_FOREACH(snake.body, first, next, cur) {
        snake_body_segment *cur_seg = cur->value;
        //if position matches and it's not the head
        if(head->x == cur_seg->x && head->y == cur_seg->y && head != cur_seg) {
            return true;
        }
    }
    return false;
}