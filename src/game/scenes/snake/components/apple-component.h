#pragma once
/** @file */
#include "../../../../containers/lcthw-list.h"

#include "limiter.h"
#include <allegro5/allegro.h>

/// @brief apple has coordinates and points
typedef struct {
    unsigned points;
    int x;
    int y;
} apple;

/// @brief apples contains list of associated apples and bitmap for them
typedef struct {
    ALLEGRO_BITMAP *bitmap; 
    List *list;
} apples;

#include "snake-data.h"

enum {
    /// @brief minimal apple points
    MIN_APPLE_POINTS = 1,
    /// @brief maximal apple points
    MAX_APPLE_POINTS = 5,
    /// @brief multiplier for apple points
    APPLE_POINTS_MULTIPLIER = 10
};

enum {
    /// @brief apple size in pixels
    APPLE_SIZE = 32
};

/// @return apple in given @param limiter box
apple spawn_random_apple(limiter limiter);

/// @return initializated apples component with associated @param  limiter
apples spawn_apples(limiter limiter);

/// @brief frees given @param apples
void free_apples(apples apples);

/// @brief pushes given @param apple to @param apples component
void push_apple(apple appl, apples apples);

/// @brief draws apples of given @param apples component
void draw_apples(apples apples);

enum {
    /// @brief apple spawner ticks
    APPLE_SPAWNER_TICKS_FOR_EVENT = 30
};

/// @brief apples spawner
void update_apple_spawner(apples apples, snake snake, limiter limiter);