#pragma once

/** @file */
/* @brief box limiter */
typedef struct {
    int min_x;
    int max_x;
    int min_y;
    int max_y;
} limiter;