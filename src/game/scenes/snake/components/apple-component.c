#include "apple-component.h"
/** @file */
#include <stdlib.h>
#include <stdio.h>

apple spawn_random_apple(limiter limiter) {
    //apple
    apple result;

    //random pos x
    result.x = (rand() % (limiter.max_x-1)) + limiter.min_x;
    //random pos y
    result.y = (rand() % (limiter.max_y-1)) + limiter.min_y;
    //random points in given range
    result.points = ((rand() % MAX_APPLE_POINTS) + MIN_APPLE_POINTS) * APPLE_POINTS_MULTIPLIER;
     //returns random apple
    return result;
}

apples spawn_apples(limiter limiter) {
    //creates apples component
    apples result = {
        //creates apples list
        .list = List_create(),
        //loads apple bitmap
        .bitmap = al_load_bitmap("resources/images/apple.png")
    };

    //returns apples component
    return result;
}

void free_apples(apples apples) {
    //destroys apple bitmap
    al_destroy_bitmap(apples.bitmap);
    //frees apples
    List_destroy(apples.list);
}

void push_apple(apple appl, apples apples) {
    //alloocates memory
    apple *mem = malloc(sizeof(apple));
    //copy value
    *mem = appl;

    //push allocated apple
    List_push(apples.list, mem);
}

void draw_apples(apples apples) {
    //draws each apple from apples.list
    LIST_FOREACH(apples.list, first, next, current) {
        apple *apple = current->value;
        al_draw_bitmap(apples.bitmap, apple->x*APPLE_SIZE, apple->y*APPLE_SIZE, 0);
    }
}

static bool snake_contains_pos(snake snake, int x, int y) {
    LIST_FOREACH(snake.body, first, next, cur) {
        snake_body_segment *seg = cur->value;

        if(seg->x == x && seg->y == y) {
            return true;
        }
    }
    return false;
}

void update_apple_spawner(apples apples, snake snake, limiter limiter) {
    static int counter = 0;
    counter = (counter % APPLE_SPAWNER_TICKS_FOR_EVENT) + 1;
    if(counter == 1) {
        //if there are no apples left
        if(apples.list->count == 0) {
            //pushes random apple on timer event
            apple random_apple;
            do {
                random_apple = spawn_random_apple(limiter);
            } while(snake_contains_pos(snake, random_apple.x, random_apple.y));
            printf("randomized apple at x{%d} y{%d}\n", random_apple.x, random_apple.y);
            push_apple(random_apple, apples);
        }
    }
}