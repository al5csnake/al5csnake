#include "game-snake-scene.h"
/** @file */
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>

#include <malloc.h>
#include <stdio.h>

#include "components/snake-component.h"
#include "components/apple-component.h"

#include "../game-over/game-over-scene.h"

typedef struct {
    //scenes_manager needed to push game_over_scene
    engi_scenes_manager *scenes_manager;

    //font for points drawing
    ALLEGRO_FONT *font;
    //background bitmap
    ALLEGRO_BITMAP *background;
    //topbar bitmap
    ALLEGRO_BITMAP *topbar;

    //snake component
    snake snake;
    //event queue (can be removed)
    engi_event_queue *queue;

    //apples component
    apples apples;

    //world limiter (for snake moving & apples spawning)
    limiter world_limiter;
} game_snake_data;

static game_snake_data *create_game_snake_data(engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //allocates memory
    game_snake_data *result = malloc(sizeof(game_snake_data));
    //loads font
    result->font = al_load_ttf_font("resources/fonts/Roboto-Black.ttf", 27, 0);
    //loads background bitmap
    result->background = al_load_bitmap("resources/images/background.jpg");
    //loads topbar bitmap
    result->topbar = al_load_bitmap("resources/images/topbar.jpg");
    //assigns event queue
    result->queue = queue;
    //assigns scenes_manager
    result->scenes_manager = scenes_manager;

    //creates limiter
    limiter lmtr = {
        .min_x = 0,
        .min_y = 1,
        .max_x = 640/32,
        .max_y = 480/32
    };
    //assigns created limiter
    result->world_limiter = lmtr;

    //spawns snake with 4 segments, going down
    result->snake = spawn_snake(4, way_down);
    //spawns apples
    result->apples = spawn_apples(lmtr);
    //returns game_snake_data
    return result;
}

static void destroy_game_snake_scene(void *data) {
    //cast
    game_snake_data *gsd = data;
    //destroys font
    al_destroy_font(gsd->font);
    //destroys background bitmap
    al_destroy_bitmap(gsd->background);
    //destroys topbar bitmap
    al_destroy_bitmap(gsd->topbar);
    
    //frees snake
    free_snake(gsd->snake);

    //frees apples
    free_apples(gsd->apples);
    //releases game snake data memory
    free(gsd);
}


static void render(void *data) {
    //cast
    game_snake_data *gsd = data;

    //draws background at position {0, 0}
    al_draw_bitmap(gsd->background, 0, 0, 0);
    //draws topbar at position {0, 0}
    al_draw_bitmap(gsd->topbar, 0, 0, 0);

    //creates points: {amount} message
    char message[64];
    sprintf(message, "Points: %d", gsd->snake.points);
    //draws message with white color at position {0, 0}
    al_draw_text(gsd->font, al_map_rgb(255, 255, 255), 0, 0, 0, message);

    //draws apples
    draw_apples(gsd->apples);
    //draws snake
    draw_snake(gsd->snake);
}

static void update(ALLEGRO_EVENT ev, bool *exit, void *data) {
    //cast
    game_snake_data *gsd = data;

    //updates snake move timer
    update_snake_mover(gsd->snake);
    //updates apple spawn timer
    update_apple_spawner(gsd->apples, gsd->snake, gsd->world_limiter);
    //eats apple if one is touched by head
    find_and_eat_touched_apple(&gsd->snake, gsd->apples);

    //quit
    bool quit = false;
    //if snake is outside of world box
    quit |= !test_snake_in_box(gsd->snake, gsd->world_limiter);
    //or if snake touches himself
    quit |= snake_touches_himself(gsd->snake);

    //if we're about to quit
    if(quit) {
        //mark loop as ended
        *exit = true;
        //creates input for game_over_scene
        game_over_scene_input input = {
            .points = gsd->snake.points
        };
        //run game_over_scene
        push_scene_to_engi_scenes_manager(gsd->scenes_manager, create_game_over_scene(gsd->queue, input));
    }

    //on key down
    if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
        switch(ev.keyboard.keycode) {
            //exit game and go back to main menu if esc is pressed
        case ALLEGRO_KEY_ESCAPE:
            *exit = true;
            break;
            //up arrow: snake tries to go up
        case ALLEGRO_KEY_UP:
            set_snake_way(&gsd->snake, way_up);
            break;
            //down arrow: snake tries to go down
        case ALLEGRO_KEY_DOWN:
            set_snake_way(&gsd->snake, way_down);
            break;
            //left arrow: snake tries to go left
        case ALLEGRO_KEY_LEFT:
            set_snake_way(&gsd->snake, way_left);
            break;
            //right arrow: snake tries to go right
        case ALLEGRO_KEY_RIGHT:
            set_snake_way(&gsd->snake, way_right);
            break;
        }
    }
}


engi_scene *create_game_snake_scene(engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //assigns loop_data
    engi_loop_data eld = {
        .event_queue = queue,
        .renderfunc = render,
        .updatefunc = update
    };

    //returns new game_snake_scene
    return create_engi_scene(eld, destroy_game_snake_scene, create_game_snake_data(queue, scenes_manager));
}