#include "game-menu-scene.h"
/** @file */
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>

#include <malloc.h>
#include <stdio.h>

#include "../snake/game-snake-scene.h"

typedef struct {
    ALLEGRO_FONT *font;

    ALLEGRO_BITMAP *background_bitmap;
    engi_event_queue *queue;
    unsigned position;
    engi_scenes_manager *scenes_manager;
} game_menu_data;


static void start_game(bool *exit, engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //run snake_game scene
    push_scene_to_engi_scenes_manager(scenes_manager, create_game_snake_scene(queue, scenes_manager));
}

static void exit_game(bool *exit, engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //exit menu scene
    *exit = true;
}

//binded menu positions
static const struct {
    const char *name;
    void(*action)(bool *exit, engi_event_queue *queue, engi_scenes_manager *scenes_manager);
} menu_elements[] = {
    { "New Game", start_game },
    { "Exit Game", exit_game }
};

//data for elements rendering
static const unsigned el_count = sizeof(menu_elements) / sizeof(menu_elements[0]);
static const unsigned x_off = 100, y_off = 100;
static const unsigned interval = 60;


static game_menu_data *create_game_menu_data(engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //allocates memory
    game_menu_data *result = malloc(sizeof(game_menu_data));
    //loads font
    result->font = al_load_ttf_font("resources/fonts/Roboto-Black.ttf", 50, 0);
    //loads background
    result->background_bitmap = al_load_bitmap("resources/images/menu-bg.jpg");
    //assigns queue
    result->queue = queue;
    //inits position to 0 (top element)
    result->position = 0;
    //assigns scenes_manager
    result->scenes_manager = scenes_manager;
    //returns new game_menu_data
    return result;
}

static void destroy_game_menu_scene(void *data) {
    //cast
    game_menu_data *gmd = data;

    //destroys font
    al_destroy_font(gmd->font);

    //destroys background
    al_destroy_bitmap(gmd->background_bitmap);

    //releases game menu data
    free(gmd);
}


static void render(void *data) {
    //cast
    game_menu_data *gmd = data;

    //draws background
    al_draw_bitmap(gmd->background_bitmap, 0, 0, 0);

    //renders all menu elements
    unsigned i = 0;
    for(i; i < el_count; ++i) {
        //green color
        ALLEGRO_COLOR green = al_map_rgb(100, 255, 100);
        //white color
        ALLEGRO_COLOR white = al_map_rgb(255, 255, 255);
        //color for current element
        ALLEGRO_COLOR color = (i == gmd->position? green : white);
        //draws element
        al_draw_text(gmd->font, color, x_off, y_off + interval*i, 0, menu_elements[i].name);
    }
}

static void update(ALLEGRO_EVENT ev, bool *exit, void *data) {
    //cast
    game_menu_data *gmd = data;

    //on key down
    if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
        switch(ev.keyboard.keycode) {
            //arrow up
        case ALLEGRO_KEY_UP:
            if(gmd->position > 0) 
                gmd->position -= 1;
            break;
            //arrow down
        case ALLEGRO_KEY_DOWN:
            if(gmd->position < el_count-1)
                gmd->position += 1;
            break;
            //enter
        case ALLEGRO_KEY_ENTER:
            menu_elements[gmd->position].action(exit, gmd->queue, gmd->scenes_manager);
            break;
        }
    }
}


engi_scene *create_game_menu_scene(engi_event_queue *queue, engi_scenes_manager *scenes_manager) {
    //assigs loop_data
    engi_loop_data eld = {
        .event_queue = queue,
        .renderfunc = render,
        .updatefunc = update
    };

    //returns new game_menu_scene
    return create_engi_scene(eld, destroy_game_menu_scene, create_game_menu_data(queue, scenes_manager));
}