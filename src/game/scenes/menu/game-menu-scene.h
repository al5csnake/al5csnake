#pragma once
/** @file */
#include "../../../engine/scenes/scene/engi-scene.h"
#include "../../../engine/scenes/manager/engi-scenes-manager.h"

/**
 * @return new game_menu_scene
 */
engi_scene *create_game_menu_scene(engi_event_queue *queue, engi_scenes_manager *scenes_manager);