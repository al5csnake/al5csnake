#pragma once
/** @file */
#include "../../../engine/scenes/scene/engi-scene.h"
#include "../../../engine/scenes/manager/engi-scenes-manager.h"

typedef struct {
    unsigned points;
} game_over_scene_input;

/// @return new game_over_scene with given @param input
engi_scene *create_game_over_scene(engi_event_queue *queue, game_over_scene_input input);