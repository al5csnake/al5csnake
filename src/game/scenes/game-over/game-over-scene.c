#include "game-over-scene.h"
/** @file */
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>

typedef struct {
    //big font
    ALLEGRO_FONT *big_font;
    //small font
    ALLEGRO_FONT *small_font;
    //event queue (can be removed)
    engi_event_queue *queue;
    //input from outside
    game_over_scene_input input;
} game_over_data;


/// @return new game_over_scene with given @param input
static game_over_data *create_game_over_data(engi_event_queue *queue, game_over_scene_input input) {
    //allocates memory
    game_over_data *result = malloc(sizeof(game_over_data));
    //loads big font
    result->big_font = al_load_ttf_font("resources/fonts/Roboto-Black.ttf", 80, 0);
    //loads small font
    result->small_font = al_load_ttf_font("resources/fonts/Roboto-Black.ttf", 30, 0);
    //assigns input
    result->input = input;
    //assigns event queue
    result->queue = queue;
    return result;
}

static void destroy_game_over_scene(void *data) {
    //cast
    game_over_data *god = data;
    //destroys big font
    al_destroy_font(god->big_font);
    //destroys small font
    al_destroy_font(god->small_font);
    free(god);
}


static void render(void *data) {
    //cast
    game_over_data *god = data;

    //draws Game over
    al_draw_text(god->big_font, al_map_rgb(255, 0, 0), 100, 120, 0, "Game over!");

    al_draw_textf(god->small_font, al_map_rgb(70, 130, 255), 120, 270, 0, "You gathered %u points!", god->input.points);

    //draws "what to do" message
    al_draw_text(god->small_font, al_map_rgb(100, 100, 100), 120, 350, 0, "Press any key to continue...");
}

static void update(ALLEGRO_EVENT ev, bool *exit, void *data) {
    //cast
    game_over_data *god = data;

    //exit scene on key down
    if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
        *exit = true;
    }
}


engi_scene *create_game_over_scene(engi_event_queue *queue, game_over_scene_input input) {
    //creates loop_data
    engi_loop_data eld = {
        .event_queue = queue,
        .renderfunc = render,
        .updatefunc = update
    };

    //returns new scene (game over scene)
    return create_engi_scene(eld, destroy_game_over_scene, create_game_over_data(queue, input));
}