#pragma once
/** @file */
#define mu_assert(test) do { if (!(test)) return #test; } while (0)

#define mu_run_test(test) do { const char *message = test(); \
                                if (message) return message; } while (0)

#define mu_stdout_test(test, suitename) do {                      \
    const char *message = test();                                 \
    if(message) {                                                 \
        printf("Error in {%s} with: {%s}\n", suitename, message); \
    } else {                                                      \
        printf("Passed {%s}", suitename);                         \
    }                                                             \
} while (0)