#include "game/snake-game.h"
/** @file */
#include "dependencies/minunit.h"

#include <stdio.h>
#include <stdlib.h>

void init() {
    engi_init();
    al_init_primitives_addon();
    al_init_image_addon();
    al_init_ttf_addon();
}

int main(int argc, char **argv) {
    srand(time(0));
    init();

    run_snake_game();
    return 0;
}