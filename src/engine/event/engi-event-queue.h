#pragma once
/** @file */
struct _engi_event_queue;
typedef struct _engi_event_queue engi_event_queue;

#include <stdbool.h>

#include "../display/engi-display.h"

enum {
    ENGI_FPS = 60
};

/**
 * @param display registers event_queue in engi_display
 * @returns new engi_event_queue, NULL if registering went wrong
 */
engi_event_queue *create_engi_event_queue(engi_display *display);

/**
* @param event_queue object to be destroyed
*/
void destroy_engi_event_queue(engi_event_queue *event_queue);

/**
 * @param event_queue the queue for event pullout
 * @return pulled allegro event
 */
ALLEGRO_EVENT pull_engi_event(engi_event_queue *event_queue);

/**
 * @param event_queue queue to check
 * @return true if queue is empty, false otherwise
 */
bool is_engi_event_queue_empty(engi_event_queue *event_queue);