#include "engi-event-queue.h"
/** @file */
#include <malloc.h>
#include <stdio.h>

#include "allegro5/allegro.h"

struct _engi_event_queue {
    //event queue
    ALLEGRO_EVENT_QUEUE *event_queue;
    //event timer
    ALLEGRO_TIMER *timer;
};

engi_event_queue *create_engi_event_queue(engi_display *display) {
    //allocates memory
    engi_event_queue *result = malloc(sizeof(engi_event_queue));

    //creates allegro event queue
    result->event_queue = al_create_event_queue();
    //if failed to create allegro event queue
    if(!result->event_queue) {
        //prints error message
        fprintf(stderr, "failed to create event queue; with: al_create_event_queue()\n");
        //releases memory
        free(result);
        //returns NULL on error
        return NULL;
    }

    //creates timer (default: ENGI_FPS)
    result->timer = al_create_timer(1.0 / ENGI_FPS);

    //if failed to create allegro timer
    if(!result->timer) {
        //prints error message
        fprintf(stderr, "failed to create event queue! with: al_create_timer(1.0 / ENGI_FPS)\n");
        //releases memory
        free(result);
        //returns NULL on error
        return NULL;
    }

    //registers display_event_source for event_queue
    al_register_event_source(result->event_queue, get_engi_display_event_source(display));

    //registers timer_event_source for event_queue
    al_register_event_source(result->event_queue, al_get_timer_event_source(result->timer));

    //registers keyboard_event_source for event_queue
    al_register_event_source(result->event_queue, al_get_keyboard_event_source());

    //starts timer immediately
    al_start_timer(result->timer);

    //returns event_queue
    return result;
}

void destroy_engi_event_queue(engi_event_queue *event_queue) {
    //releases internal allegro event_queue
    al_destroy_event_queue(event_queue->event_queue);
    //releases memory
    free(event_queue);
}

ALLEGRO_EVENT pull_engi_event(engi_event_queue *event_queue) {
    //result for pulling
    ALLEGRO_EVENT result;
    //waits for event
    al_wait_for_event(event_queue->event_queue, &result);
    //return event
    return result;
}

bool is_engi_event_queue_empty(engi_event_queue *event_queue) {
    //tests if event_queue is empty
    return al_is_event_queue_empty(event_queue->event_queue);
}