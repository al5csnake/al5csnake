#pragma once
/** @file */
#include <stddef.h>
#include <allegro5/allegro.h>

struct _engi_display;
typedef struct _engi_display engi_display;

/**
 * @param width display width
 * @param height display height
 * @return new engi_display instance if object was successfully created, otherwise NULL
 */
engi_display *create_engi_display(size_t width, size_t height);

/**
 * @param display object to be destroyed
*/
void destroy_engi_display(engi_display *display);

/**
 * @param display object for event_source extraction
 * @return event source of display object
*/
ALLEGRO_EVENT_SOURCE *get_engi_display_event_source(engi_display *display);

/**
 * flips internal buffer
*/
void engi_flip_display_buffer();

/**
 * clears internal buffer with given RGB color
*/
void engi_clear_display_buffer(uint8_t r, uint8_t g, uint8_t b);