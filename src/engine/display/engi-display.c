#include "engi-display.h"
/** @file */
#include <malloc.h>
#include <stdio.h>

struct _engi_display {
    //allegro display internals
    ALLEGRO_DISPLAY *display;
};

engi_display *create_engi_display(size_t width, size_t height) {
    //allocate memory
    engi_display *result = malloc(sizeof(engi_display));

    //create internal allegro display
    result->display = al_create_display(width, height);

    //if allegro display creation went wrong
    if(!result->display) {
        //print error message
        fprintf(stderr, "Failed to create display; with: al_create_display(%zu, %zu)\n", width, height);
        //free allocated memory
        free(result);
        //return null
        return NULL;
    }

    //return result: created engi_display
    return result;
}

void destroy_engi_display(engi_display *display) {
    //destroy allegro internal display
    al_destroy_display(display->display);
    //free display memory
    free(display);
}

ALLEGRO_EVENT_SOURCE *get_engi_display_event_source(engi_display *display) {
    //return event source of internal allegro display
    return al_get_display_event_source(display->display);
}

void engi_flip_display_buffer() {
    //flips allegro display
    al_flip_display();
}

void engi_clear_display_buffer(uint8_t r, uint8_t g, uint8_t b) {
    //clears allegro display buffer with given RGB color
    al_clear_to_color(al_map_rgb(r, g, b));
}