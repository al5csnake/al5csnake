#include "engi-core.h"
/** @file */
#include "allegro5/allegro.h"

#include <stdio.h>

bool engi_init() {
    //tries to initialize allegro
    if(!al_init()) {
        //if it fails, print proper error message
        fprintf(stderr, "Failed to initialize allegro! with: al_init()\n");
        //and return error state
        return false;
    }

    //tries to install allegro keyboard
    if(!al_install_keyboard()) {
        //if it fails, print proper error message
        fprintf(stderr, "Failed to initialize allegro! with: al_install_keyboard()\n");
        //and return error state
        return false;
    }

    //everything went fine, so return true
    return true;
}