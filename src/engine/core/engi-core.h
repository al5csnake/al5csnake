#pragma once
/** @file */
#include <stdbool.h>

/**
 * brief: intializes internal engi context
 * @return true on success, false on failure
*/
bool engi_init();