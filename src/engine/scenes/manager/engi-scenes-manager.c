#include "engi-scenes-manager.h"
/** @file */
struct _engi_scenes_manager {
    List *scenes_stack;
};

engi_scenes_manager *create_engi_scenes_manager() {
    //allocates memory
    engi_scenes_manager *result = malloc(sizeof(engi_scenes_manager));
    //creates scenes_stack
    result->scenes_stack = List_create();
    //returns new scenes_manager
    return result;
}

void destroy_engi_scenes_manager(engi_scenes_manager *manag) {
    //releases scenes_stack
    List_clear_destroy(manag->scenes_stack);
    //releases memory
    free(manag);
}

void push_scene_to_engi_scenes_manager(engi_scenes_manager *manag, engi_scene *scene) {
    //pushes scene to scenes_stack
    List_push(manag->scenes_stack, scene);
    //runs pushed scene
    engi_game_loop(get_engi_scene_loop_data(scene), get_engi_scene_user_data(scene));
    //pops scene
    pop_scene_from_scenes_manager(manag);
}

void pop_scene_from_scenes_manager(engi_scenes_manager *manag) {
    //destroys popped scene
    destroy_engi_scene(List_pop(manag->scenes_stack));
}