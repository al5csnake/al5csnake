#pragma once
/** @file */
#include "../../../containers/lcthw-list.h"
#include "../scene/engi-scene.h"

/**
 * @brief hidden engi_scenes_manager implementation
 */
typedef struct _engi_scenes_manager engi_scenes_manager;

/**
 * @return new engi_scenes_manager
 */
engi_scenes_manager *create_engi_scenes_manager();

/**
 * destroys given @param scenes_manager
 */
void destroy_engi_scenes_manager(engi_scenes_manager *manag);

/**
 * @brief pushes @param scene to given @param scenes_manager
 */
void push_scene_to_engi_scenes_manager(engi_scenes_manager *manag, engi_scene *scene);

/**
 * @brief pops scene from top of @param scenes_manager scenes stack
 */
void pop_scene_from_scenes_manager(engi_scenes_manager *manag);