#include "engi-scene.h"
/** @file */
#include <malloc.h>

typedef struct _engi_scene {
    void *user_data;
    engi_loop_data loop_data;
    engi_scene_destroy destroyfunc;
} engi_scene;

engi_scene *create_engi_scene(engi_loop_data data, engi_scene_destroy destroyfunc, void *user_data) {
    //allocates memory
    engi_scene *result = malloc(sizeof(engi_scene));
    //assigns given parameters
    result->user_data = user_data;
    result->loop_data = data;
    result->destroyfunc = destroyfunc;
    //returns result
    return result;
}

engi_loop_data *get_engi_scene_loop_data(engi_scene *scene) {
    //returns scene loop_data
    return &scene->loop_data;
}

void *get_engi_scene_user_data(engi_scene *scene) {
    //returns scene user_data
    return scene->user_data;
}

void destroy_engi_scene(engi_scene *scene) {
    //destroys user_data
    scene->destroyfunc(scene->user_data);
    //releases memory
    free(scene);
}