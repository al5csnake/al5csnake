#pragma once
/** @file */
#include "../../game-loop/engi-game-loop.h"

/**
 * @brief defines pointer for scene destroying
 */
typedef void(*engi_scene_destroy)(void *user_data);

/**
 * @brief hidden engi_scene implementation
 */
typedef struct _engi_scene engi_scene;

/**
 * @brief creates engi_scene
 * @param data loop_data
 * @param destroyfunc external scene destructor
 * @param user_data userdata
 * @return newly allocated and initializated engi_scene
 */
engi_scene *create_engi_scene(engi_loop_data data, engi_scene_destroy destroyfunc, void *user_data);

/**
 * @return loop_data associated to given @param scene
 */
engi_loop_data *get_engi_scene_loop_data(engi_scene *scene);

/**
 * @return user_data associated to given @param scene
 */
void *get_engi_scene_user_data(engi_scene *scene);

/**
 * @brief destroys given @param scene
 */
void destroy_engi_scene(engi_scene *scene);