#pragma once
/** @file */
#include "../display/engi-display.h"
#include "../event/engi-event-queue.h"

/**
 * @brief pointer to function for loop rendering; user_data is provided from outside 
 */
typedef void(*engi_loop_render)(void *user_data);

/**
 * @brief pointer to function for loop updating; user_data is provided from outside
 */
typedef void(*engi_loop_update)(ALLEGRO_EVENT alevent, bool *exit, void *user_data);

/**
 * @brief defines set of data needed for looping the game
 */
typedef struct {
    /// event_queue for events
    engi_event_queue *event_queue;
    /// renderfuncs for rendering
    engi_loop_render renderfunc;
    /// updatefunc for updating
    engi_loop_update updatefunc;
} engi_loop_data;

/**
 * @brief loops the game using loop_data and allegro internals. user_data is passed to loop_data::{renderfunc, updatefunc}
 */
void engi_game_loop(engi_loop_data *loop_data, void *user_data);