#include "engi-game-loop.h"
/** @file */
#include <stdio.h>


void engi_game_loop(engi_loop_data *loop_data, void *user_data) {
    //loop_data must contain data
    assert(loop_data->event_queue);
    assert(loop_data->renderfunc);
    assert(loop_data->updatefunc);

    //we don't want to finish immediately
    bool exit = false;

    //as long as we want to loop
    while(!exit) {
        //pull event
        ALLEGRO_EVENT alevent = pull_engi_event(loop_data->event_queue);
        //update states
        loop_data->updatefunc(alevent, &exit, user_data);
        //on timer
        if(alevent.type == ALLEGRO_EVENT_TIMER) {
            //clear display buffer
            engi_clear_display_buffer(0, 0, 0);

            //render to display
            loop_data->renderfunc(user_data);

            //flip display buffer
            engi_flip_display_buffer();
        }
    }
}