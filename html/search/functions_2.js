var searchData=
[
  ['engi_5fclear_5fdisplay_5fbuffer',['engi_clear_display_buffer',['../engi-display_8c.html#aea2b8d4379c605a70444b4290478f688',1,'engi_clear_display_buffer(uint8_t r, uint8_t g, uint8_t b):&#160;engi-display.c'],['../engi-display_8h.html#aea2b8d4379c605a70444b4290478f688',1,'engi_clear_display_buffer(uint8_t r, uint8_t g, uint8_t b):&#160;engi-display.c']]],
  ['engi_5fflip_5fdisplay_5fbuffer',['engi_flip_display_buffer',['../engi-display_8c.html#a64f2f82ca543997ffecddac1e9c88f55',1,'engi_flip_display_buffer():&#160;engi-display.c'],['../engi-display_8h.html#a64f2f82ca543997ffecddac1e9c88f55',1,'engi_flip_display_buffer():&#160;engi-display.c']]],
  ['engi_5fgame_5floop',['engi_game_loop',['../engi-game-loop_8c.html#a0a1290933b6b6f79a943b96fb07d3e72',1,'engi_game_loop(engi_loop_data *loop_data, void *user_data):&#160;engi-game-loop.c'],['../engi-game-loop_8h.html#a0a1290933b6b6f79a943b96fb07d3e72',1,'engi_game_loop(engi_loop_data *loop_data, void *user_data):&#160;engi-game-loop.c']]],
  ['engi_5finit',['engi_init',['../engi-core_8c.html#a045a68e728c6ad325df692722ecc02ed',1,'engi_init():&#160;engi-core.c'],['../engi-core_8h.html#a045a68e728c6ad325df692722ecc02ed',1,'engi_init():&#160;engi-core.c']]]
];
