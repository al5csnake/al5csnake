var searchData=
[
  ['set_5fsnake_5fway',['set_snake_way',['../snake-component_8c.html#acce67aa0936fcba0c950e55d5f1b8d22',1,'set_snake_way(snake *snake, way way):&#160;snake-component.c'],['../snake-component_8h.html#acce67aa0936fcba0c950e55d5f1b8d22',1,'set_snake_way(snake *snake, way way):&#160;snake-component.c']]],
  ['snake',['snake',['../structsnake.html',1,'']]],
  ['snake_2dcomponent_2ec',['snake-component.c',['../snake-component_8c.html',1,'']]],
  ['snake_2dcomponent_2eh',['snake-component.h',['../snake-component_8h.html',1,'']]],
  ['snake_2ddata_2eh',['snake-data.h',['../snake-data_8h.html',1,'']]],
  ['snake_2dgame_2ec',['snake-game.c',['../snake-game_8c.html',1,'']]],
  ['snake_2dgame_2eh',['snake-game.h',['../snake-game_8h.html',1,'']]],
  ['snake_5fbody_5fsegment',['snake_body_segment',['../structsnake__body__segment.html',1,'']]],
  ['snake_5fmover_5fticks_5ffor_5fevent',['SNAKE_MOVER_TICKS_FOR_EVENT',['../snake-component_8h.html#a61dadd085c1777f559549e05962b2c9ea347eb2fe2ea1bb3b4853ac3cefda8221',1,'snake-component.h']]],
  ['snake_5fsegment_5fsize',['SNAKE_SEGMENT_SIZE',['../snake-data_8h.html#a726ca809ffd3d67ab4b8476646f26635a1006fe7b9908875b2f6c96350318f11b',1,'snake-data.h']]],
  ['spawn_5fapples',['spawn_apples',['../apple-component_8c.html#adb16e5a2b11c657b909d1976a3736e1c',1,'spawn_apples(limiter limiter):&#160;apple-component.c'],['../apple-component_8h.html#adb16e5a2b11c657b909d1976a3736e1c',1,'spawn_apples(limiter limiter):&#160;apple-component.c']]],
  ['spawn_5frandom_5fapple',['spawn_random_apple',['../apple-component_8c.html#a0c69d33c922835386e911c5c15073c80',1,'spawn_random_apple(limiter limiter):&#160;apple-component.c'],['../apple-component_8h.html#a0c69d33c922835386e911c5c15073c80',1,'spawn_random_apple(limiter limiter):&#160;apple-component.c']]],
  ['spawn_5fsnake',['spawn_snake',['../snake-component_8c.html#ab8ac235a7319112fb50407f2f6961a1d',1,'spawn_snake(unsigned segments_count, way way):&#160;snake-component.c'],['../snake-component_8h.html#ab8ac235a7319112fb50407f2f6961a1d',1,'spawn_snake(unsigned segments_count, way way):&#160;snake-component.c']]]
];
