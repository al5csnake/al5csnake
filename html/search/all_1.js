var searchData=
[
  ['apple',['apple',['../structapple.html',1,'']]],
  ['apple_2dcomponent_2ec',['apple-component.c',['../apple-component_8c.html',1,'']]],
  ['apple_2dcomponent_2eh',['apple-component.h',['../apple-component_8h.html',1,'']]],
  ['apple_5fpoints_5fmultiplier',['APPLE_POINTS_MULTIPLIER',['../apple-component_8h.html#a99fb83031ce9923c84392b4e92f956b5aee95c737dabd8a69663a5af416cd3fb5',1,'apple-component.h']]],
  ['apple_5fsize',['APPLE_SIZE',['../apple-component_8h.html#abc6126af1d45847bc59afa0aa3216b04a05361248de15d4a707af54d19d23ccc2',1,'apple-component.h']]],
  ['apple_5fspawner_5fticks_5ffor_5fevent',['APPLE_SPAWNER_TICKS_FOR_EVENT',['../apple-component_8h.html#adc29c2ff13d900c2f185ee95427fb06ca7c9911e394d07ebaf80271fdea8743d8',1,'apple-component.h']]],
  ['apples',['apples',['../structapples.html',1,'']]]
];
