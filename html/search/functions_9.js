var searchData=
[
  ['set_5fsnake_5fway',['set_snake_way',['../snake-component_8c.html#acce67aa0936fcba0c950e55d5f1b8d22',1,'set_snake_way(snake *snake, way way):&#160;snake-component.c'],['../snake-component_8h.html#acce67aa0936fcba0c950e55d5f1b8d22',1,'set_snake_way(snake *snake, way way):&#160;snake-component.c']]],
  ['spawn_5fapples',['spawn_apples',['../apple-component_8c.html#adb16e5a2b11c657b909d1976a3736e1c',1,'spawn_apples(limiter limiter):&#160;apple-component.c'],['../apple-component_8h.html#adb16e5a2b11c657b909d1976a3736e1c',1,'spawn_apples(limiter limiter):&#160;apple-component.c']]],
  ['spawn_5frandom_5fapple',['spawn_random_apple',['../apple-component_8c.html#a0c69d33c922835386e911c5c15073c80',1,'spawn_random_apple(limiter limiter):&#160;apple-component.c'],['../apple-component_8h.html#a0c69d33c922835386e911c5c15073c80',1,'spawn_random_apple(limiter limiter):&#160;apple-component.c']]],
  ['spawn_5fsnake',['spawn_snake',['../snake-component_8c.html#ab8ac235a7319112fb50407f2f6961a1d',1,'spawn_snake(unsigned segments_count, way way):&#160;snake-component.c'],['../snake-component_8h.html#ab8ac235a7319112fb50407f2f6961a1d',1,'spawn_snake(unsigned segments_count, way way):&#160;snake-component.c']]]
];
